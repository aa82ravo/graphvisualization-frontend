export const environment = {
  production: true,
  apiUrl: 'ws://localhost:8080/graphData',
  controlUrl: 'ws://localhost:8081/control'
};
