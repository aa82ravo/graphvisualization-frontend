import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { DataService } from '../core/data.service';


/**
 * SidenavComponent class provides the main header and the sidebar as a home page.
 */

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit, OnDestroy {

  Query: MediaQueryList;

  private queryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher, public dataService: DataService) {
    this.Query = media.matchMedia('(max-width: 600px)');
    this.queryListener = () => changeDetectorRef.detectChanges();
    // tslint:disable-next-line: deprecation
    this.Query.addListener(this.queryListener);
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    // tslint:disable-next-line: deprecation
    this.Query.removeListener(this.queryListener);
  }

}
