import { Component, OnInit } from '@angular/core';
import { DataService } from '../core/data.service';

/**
 * class RawDataComponent is responsible for display raw data of the graph (vertices and edges ) as JSON object.
 *
 */

@Component({
  selector: 'app-raw-data',
  templateUrl: './raw-data.component.html',
  styleUrls: ['./raw-data.component.css']
})
export class RawDataComponent implements OnInit {

  data: any;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.data = this.dataService.data;
  }

  refresh() {
    this.data = this.dataService.data;
  }

}
