import { Injectable } from '@angular/core';
import { WebSocketSubject, webSocket } from 'rxjs/websocket';
import { tap, retryWhen, delay } from 'rxjs/operators';
import { environment } from '../../environments/environment';

/**
 * DataService class is an Injectable class that works as service in order to manage WebSocket connections:
 * initial the socket beside the send/listen callbacks.
 */
@Injectable({
  providedIn: 'root'
})
export class DataService {
  public connected = false;
  public dataSocket: any;
  public controlSocket: WebSocketSubject<any>;
  public data: any;
  windowUnit = '.seconds';
  slideUnit = '.seconds';
  windowPeriod = 30;
  slidePeriod = 10;
  checked = false;
  isLoading = true;
  graphVersion;

  constructor() {
    this.dataSocket = new WebSocketSubject({
      url: environment.apiUrl,
      deserializer: ({ data }) => data
    }).asObservable().pipe(
      retryWhen(errors =>
        errors.pipe(
          tap(err => {
            console.error('Got error', err);
            this.connected = false;
          }),
          delay(60000)
        )
      )
    );
    this.controlSocket = new WebSocketSubject({
      url: environment.controlUrl,
      deserializer: ({ data }) => data
    });
  }

  public send(message: any) {
    this.controlSocket.next(message);
  }

  public listen(callback: { (data: string): void; (arg0: string): void; }) {
    this.dataSocket.subscribe(data => {
      this.connected = true;
      callback(data);
    }, (err: any) => console.error(err));

    this.controlSocket.subscribe(data => {
      console.log(data);
    }, (err: any) => console.error(err));
  }
}
