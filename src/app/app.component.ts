import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from './core/data.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  stockQuote: string;
  sub: Subscription;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    /* this.dataService.listen((data: string) => {
      console.log(data);
      this.stockQuote = data;
    }); */
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
