import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  option: any;
  data: Array<any>;
  edges: Array<any>;
  merge: any;
  categories: any;
  colors: any;
  sizes: number[];

  constructor() {
    this.data = new Array<any>();
    this.edges = new Array<any>();
    this.categories = [{ name: "c1" }, { name: "c2" }, { name: "c3" }];
    this.colors = ["#262A31", "#802F2C", "#D8413E"]
    this.sizes = [10, 50];
  }

  ngOnInit() {
    for (let i = 0; i < 5000; i++) {
      const element = {
        name: i.toString(), itemStyle: {
          color: this.colors[i % 3]
        }, symbolSize: undefined
      };
      if (this.randomIntFromInterval(1, 100) < 6) {
        element.symbolSize = 50;
      }
      if (this.randomIntFromInterval(1, 1000) < 5) {
        element.symbolSize = 150;
      }
      this.data.push(element);
    }
    for (let index = 0; index < this.data.length; index++) {
      const element = this.data[index];
      let anotherPoint = this.randomIntFromInterval(0, this.data.length - 1);
      this.edges.push({
        source: element.name,
        target: this.data[anotherPoint].name
      });

    }
    this.option = {
      /* title: {
        text: 'Project Distribution',
        subtext: 'Default layout',
        top: 'bottom',
        left: 'right'
      }, */
      /* tooltip: {},
      animationDuration: 1500,
      animationEasingUpdate: 'quinticInOut', */
      legend: [{
        data: this.categories.map(a => {
          return a.name;
        })
      }],
      series: [{
        /* edgeSymbol: ['circle', 'arrow'], */
        edgeSymbolSize: [4, 5],
        /* categories: this.categories, */
        /* label: {
          position: 'right',
          show: true,
          formatter: '{b}'
        }, */
        /* focusNodeAdjacency: true, */
         lineStyle: {
          color: 'source',
          curveness: 0.3
        }, 
        itemStyle: {
          normal: {
            borderColor: '#fff',
            borderWidth: 1,
            shadowBlur: 10,
            shadowColor: 'rgba(0, 0, 0, 0.3)'
          }
        },
        type: 'graph',
        layout: 'force',
        animation: false,
        emphasis: {
          lineStyle: {
            width: 10
          }
        },
        /* force: {
          repulsion: 400,
          gravity: 0.2,
        }, */

      }]
    };
    this.merge = {
      series: [{
        roam: true,
        data: this.data,
        edges: this.edges
      }]
    };
  }
  randomIntFromInterval(min, max) { // min and max included 
    return Math.floor(Math.random() * (max - min + 1) + min);
  }
}
