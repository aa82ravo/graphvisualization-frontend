import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DataService } from '../core/data.service';
import { MatSnackBar } from '@angular/material';
import { FormControl } from '@angular/forms';

export interface Unit {
  value: string;
  viewValue: string;
}

/**
 * class GraphComponent is responsible for graph visualization by using a javascript library that takes
 * graph vertices and edges as input.
 *
 */

@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.css']
})
export class GraphComponent implements OnInit {

  option: any;
  data: any;
  edges: any;
  merge: any;
  checked = false;
  windowUnit: string;
  slideUnit: string;
  windowPeriodControl = new FormControl();
  slidePeriodControl = new FormControl();
  isLoading = true;
  categories: any;

  units: Unit[] = [
    { value: '.milliseconds', viewValue: 'Milliseconds' },
    { value: '.seconds', viewValue: 'Seconds' },
    { value: '.minutes', viewValue: 'Minutes' },
    { value: '.hours', viewValue: 'Hours' }
  ];

  constructor(private http: HttpClient, private dataService: DataService, private snackBar: MatSnackBar) { }

  /*
  * The initialize phase sets the global configuration of the graph layout
  * and connects with the backend in order to receive the resulting stream as JSON.
  */
  ngOnInit() {
    this.windowPeriodControl.setValue(this.dataService.windowPeriod);
    this.slidePeriodControl.setValue(this.dataService.slidePeriod);
    this.windowUnit = this.dataService.windowUnit;
    this.slideUnit = this.dataService.slideUnit;
    this.checked = this.dataService.checked;
    this.dataService.graphVersion = 'Restart!' + '|' + this.checked + '|' +
    this.dataService.windowPeriod + this.windowUnit + '|' + this.dataService.slidePeriod + this.slideUnit;

    this.dataService.listen((data: string) => {
      // console.log(data);
      if (data) {
        const points = JSON.parse(data);
        if (points.version === this.dataService.graphVersion) {
          console.log('Data Service:' + this.dataService.graphVersion);
          this.categories = points.categories;
          this.dataService.isLoading = false;
          this.isLoading = this.dataService.isLoading;
          this.dataService.data = points;
          this.data = points.points;
          this.edges = points.edges;
          this.option = {
            title: {
              text: 'Project Distribution',
              subtext: 'Default layout',
              top: 'bottom',
              left: 'right'
            },
            tooltip: {},
            animationDuration: 1500,
            animationEasingUpdate: 'quinticInOut',
            legend: [{
              data: this.categories.map(a => {
                return a.name;
              })
            }],
            series: [{
              edgeSymbol: ['circle', 'arrow'],
              edgeSymbolSize: [4, 5],
              categories: this.categories,
              label: {
                position: 'right',
                show: true,
                formatter: '{b}'
              },
              focusNodeAdjacency: true,
              lineStyle: {
                color: 'source',
                curveness: 0.3
              },
              itemStyle: {
                normal: {
                  borderColor: '#fff',
                  borderWidth: 1,
                  shadowBlur: 10,
                  shadowColor: 'rgba(0, 0, 0, 0.3)'
                }
              },
              type: 'graph',
              layout: 'force',
              animation: false,
              emphasis: {
                lineStyle: {
                  width: 10
                }
              },
              force: {
                repulsion: 400,
                gravity: 0.2,
              },

            }]
          };
          if (this.dataService.checked) {
            this.data.forEach(vertex => {
              this.edges.forEach(edge => {
                if (edge.source === vertex.name) {
                  edge.source = vertex.value;
                }
                if (edge.target === vertex.name) {
                  edge.target = vertex.value;
                }
              });
              vertex.name = vertex.value;
            });
          }

          this.merge = {
            series: [{
              roam: true,
              data: this.data,
              edges: this.edges
            }]
          };
        } else {
          this.dataService.data = null;
        }
      }
    });

  /*   if (this.dataService.data) {
      this.data = this.dataService.data.points;
      this.edges = this.dataService.data.edges;
      this.merge = {
        series: [{
          roam: true,
          data: this.data,
          edges: this.edges
        }]
      };
      this.isLoading = this.dataService.isLoading;
    } */
  }

  public getJSON(path: string): Observable<any> {
    return this.http.get(path);
  }

  /*
  * This method applies the requested changes on window size and sends an announcement
  * to the backend that requires a restart process
  */
  applyChanges(windowUnit: string, slideUnit: string) {
    this.dataService.isLoading = true;
    this.dataService.checked = this.checked;
    this.dataService.windowPeriod = this.windowPeriodControl.value;
    this.dataService.slidePeriod = this.slidePeriodControl.value;
    this.dataService.windowUnit = windowUnit;
    this.dataService.slideUnit = slideUnit;
    this.isLoading = this.dataService.isLoading;
    this.dataService.graphVersion = 'Restart!' + '|' + this.checked + '|' +
      this.dataService.windowPeriod + windowUnit + '|' + this.dataService.slidePeriod + slideUnit;
    console.log(this.windowPeriodControl.value + windowUnit, this.slidePeriodControl.value + slideUnit, this.checked);
    const controlMsg = {
      type: 'controlSignal',
      withGrouping: this.checked,
      windowSize: this.windowPeriodControl.value + windowUnit,
      slideSize: this.slidePeriodControl.value + slideUnit,
      timestamp: Date.now()
    };
    this.dataService.send(controlMsg);
    this.snackBar.open('Please wait until the change is applied; this may take a while.', 'OK', {
      duration: 50000,
    });
  }
}
