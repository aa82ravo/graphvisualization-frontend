import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GraphComponent } from './graph/graph.component';
import { InfoComponent } from './info/info.component';
import { RawDataComponent } from './raw-data/raw-data.component';

const routes: Routes = [
{ path: '', component: GraphComponent },
{ path: 'info', component: InfoComponent },
{ path: 'raw-data', component: RawDataComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
