import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { SidenavComponent } from './sidenav/sidenav.component';
import { MaterialModule } from './material/material.module';
import { NgxEchartsModule } from 'ngx-echarts';
import { GraphComponent } from './graph/graph.component';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { InfoComponent } from './info/info.component';
import { RawDataComponent } from './raw-data/raw-data.component';
import { NgxJsonViewerModule } from 'ngx-json-viewer';
import { TestComponent } from './test/test.component';

@NgModule({
  declarations: [
    AppComponent,
    SidenavComponent,
    GraphComponent,
    InfoComponent,
    RawDataComponent,
    TestComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MaterialModule,
    CoreModule,
    NgxEchartsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxJsonViewerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
