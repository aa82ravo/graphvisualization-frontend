import { Component, OnInit } from '@angular/core';

/**
 * class InfoComponent is responsible for give an overview of this app
 *
 */

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})
export class InfoComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
